Developers
==========

The list of active developers participating in OTREES projects. See the
list `here <https://gitlab.fel.cvut.cz/otrees/org/-/wikis/developers>`_.
.. SPDX-License-Identifier: CC-BY-SA-4.0

Home
====

The page is currently under construction. Please refer to
`old Wiki page <https://gitlab.fel.cvut.cz/otrees/org/-/wikis/home>`_ for
complete information.

.. toctree::
  :maxdepth: 2

  projects/index
  theses
  outputs/index
  courses
  developers

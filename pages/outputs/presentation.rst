Presentations
=============

The most of recorded videos are in Czech language and have been prepared and
recorded to help students, enthusiasts and even experts from companies to
understand and select right technologies for embedded and control applications,
use appropriate open source development and design tools and build sound
applications and building blocks which would service to the mankind for
decades. Many of our solutions and technologies proved to fall into this
category. See the list `here <https://gitlab.fel.cvut.cz/otrees/org/-/wikis/knowbase>`_.

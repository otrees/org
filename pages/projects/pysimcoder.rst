pysimCoder
==========

`pysimCoder <https://github.com/robertobucher/pysimCoder>`_ is a Rapid
Prototyping tool that can be used to generate a real-time code for different
target. It can be used as an open source alternative to Matlab/Simulink and
is intended mainly for education purposes, but it can be found in more serious
projects as well.

It was created by prof. Roberto Bucher from the University of Applied Sciences
and Arts of Southern Switzerland. There is available support for various
target platforms, including GNU/Linux and, thanks to OTREES projects, NuttX
real time operating system as well.

There are many possible contributions to the project from low level
C programming focused on hardware and operating systems (creation of new
blocks, control capabilities etc.) to high level Python based programming
including GUI enhancements, tuning of model parameters and so on.

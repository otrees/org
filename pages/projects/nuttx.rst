NuttX Related Projects
======================

`NuttX <https://nuttx.apache.org/>`_ is an open source real time operating
systems intended mostly for small and cheap microcontrollers.

NuttX on ESP32C3
~~~~~~~~~~~~~~~~

Work on CAN/TWAI the driver by Jan Charvát in the
`branch esp32c3-twai <https://github.com/charvj/incubator-nuttx/tree/esp32c3-twai>`_.

`ICE-V ESP32C3 <https://github.com/ICE-V-Wireless/ICE-V-Wireless>`_ with
iCE40 FPGA, kits donated by `RISC-V International <https://riscv.org>`_.
Planned is minimized motion controller support based on NuttX a iCE40 port
of our previous PMSM projects for Raspberry Pi and Xilinx Zynq.

The ongoing project to control PMSM motors by ICE-V can be found
`here <https://gitlab.fel.cvut.cz/otrees/risc-v-esp32/ice-v-pmsm>`_.

NuttX on SAMv7
~~~~~~~~~~~~~~

`SaMoCoN <https://gitlab.fel.cvut.cz/otrees/motion/samocon>`_ - NuttX and
SAMv7 open source motion/robotic controller by Štěpán Pressl.

NuttX on iMXRT
~~~~~~~~~~~~~~

NuttX provides support for `i.MX RT <https://www.nxp.com/products/processors-and-microcontrollers/arm-microcontrollers/i-mx-rt-crossover-mcus:IMX-RT-SERIES>`_.
Support for cheap `Teensy 4.x <https://www.pjrc.com/store/teensy40.html>`_
boards was added by Michal Lenc to `NuttX mainline <https://nuttx.apache.org/docs/latest/platforms/arm/imxrt/boards/teensy-4.x/index.html>`_.

FlexCAN driver for imxRT integrated was into project mainline together with
many other peripherlas (PWM, tickless mode, DMA support for ADC).

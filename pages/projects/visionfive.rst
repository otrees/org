GNU/Linux Benchmarking on VisionFive2 RISC-V Board
==================================================

Resources:

* `RVspace <https://rvspace.org/en/home>`_
* `VisionFive2 <https://github.com/starfive-tech/VisionFive2>`_

Our main focus is CAN interface benchmarking and use in RT and automotive
applications. It also includes 3D accelerated graphic with open-source drivers,
and we have contact to mesa and kernel graphic drivers developers.

CTU courses
===========

- B35APO - Computer Architectures (Architektura počítačů) - `Czech <https://cw.fel.cvut.cz/wiki/courses/b35apo/en/start>`__, `English <https://cw.fel.cvut.cz/wiki/courses/b35apo/en/start>`__ - complete recordings of lectures in `Czech language full Píša, P. <https://www.youtube.com/playlist?list=PLQL6z4JeTTQnq6kjJ9JO-Fb7Md-ofJ-zL>`__, `dense version Štěpán, P. <https://www.youtube.com/playlist?list=PLQL6z4JeTTQkguL75-cpr6tXc6atfm5m3>`__, `English lectures 2022 on YouTube <https://www.youtube.com/playlist?list=PLQL6z4JeTTQnTrML7HgagbjdpCtvdyu0M>`__, `English recording of meetings and lectures 2021 <https://cw.fel.cvut.cz/b202/courses/b35apo/en/lectures/start>`__
- B4M35PAP/BE4M35PAP - Advanced Computer Architectures (Pokročilé architektury počítačů) - `English <https://cw.fel.cvut.cz/wiki/courses/b4m35pap/start>`__, `Czech recordings 2021 <https://www.youtube.com/playlist?list=PLQL6z4JeTTQla6OFD1JAAtAt7Zw_3Ys61>`__, `English recording of meetings and lectures 2020 <https://cw.fel.cvut.cz/b201/courses/b4m35pap/lectures/start>`__ by Píša, P. with spectre and meltdown guest lecture by Kočí, K. from CZ.nic
- A4M35OSP - Open Source Programování `Czech <https://wiki.control.fel.cvut.cz/osp/>`__
- The common CTU site for Computer Architectures materials and RISC-V simulator is there `http://comparch.edu.cvut.cz/ <http://comparch.edu.cvut.cz/>`__.
